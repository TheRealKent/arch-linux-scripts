#!/usr/bin/env python3
import gi
import csv
import sys
import os

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class GridWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title=" Desktop Installer ")
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_border_width(15)

        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(4)
        self.add(grid)

        # labels
        labelXfce = Gtk.Label(label="Install XFCE Desktop", halign=Gtk.Align.START)
        labelGnome = Gtk.Label(label="Install Gnome Desktop", halign=Gtk.Align.START)
        labelLx = Gtk.Label(label="Install LXQT Desktop", halign=Gtk.Align.START)
        labelKde = Gtk.Label(label="Install KDE Desktop", halign=Gtk.Align.START)

        # buttons
        buttonXfceIn = Gtk.Button.new_with_label("Install")
        buttonXfceIn.connect("clicked", self.install_xfce)
        buttonGnomeIn = Gtk.Button(label="Install")
        buttonGnomeIn.connect("clicked", self.install_gnome)
        buttonLxIn = Gtk.Button(label="install")
        buttonLxIn.connect("clicked", self.install_lxqt)
        buttonLxRe = Gtk.Button(label="Remove")
        buttonLxRe.connect("clicked", self.remove_lxqt)
        buttonXfceRe = Gtk.Button(label="Remove")
        buttonXfceRe.connect("clicked", self.remove_xfce)
        buttonGnomeRe = Gtk.Button(label="Remove")
        buttonGnomeRe.connect("clicked", self.remove_gnome)
        buttonKdeIn = Gtk.Button(label="install")
        buttonKdeIn.connect("clicked", self.install_kde)
        buttonKdeRe = Gtk.Button(label="Remove")
        buttonKdeRe.connect("clicked", self.remove_kde)


        # Grid layout
        grid.attach(labelXfce, 0, 0, 1, 1)
        grid.attach(labelGnome, 0, 1, 1, 1)
        grid.attach(labelLx, 0, 2, 1, 1)
        grid.attach(labelKde, 0, 3, 1, 1)

        grid.attach(buttonXfceIn, 1, 0, 1, 1)
        grid.attach(buttonGnomeIn, 1, 1, 1, 1)
        grid.attach(buttonLxIn, 1, 2, 1, 1)
        grid.attach(buttonKdeIn, 1, 3, 1, 1)
        grid.attach(buttonLxRe, 2, 2, 1, 1)
        grid.attach(buttonXfceRe, 2, 0, 1, 1)
        grid.attach(buttonGnomeRe, 2, 1, 1, 1)
        grid.attach(buttonKdeRe, 2, 3, 1, 1)
    # Functions

    # ---Function install XFCE
    def install_xfce(self, button):
        os.system("xterm -e sudo pacman -S xfce4 xfce4-goodies")

    # ---Function remove XFCE
    def remove_xfce(self, button):
        os.system("xterm -e sudo pacman -Rns thunar-shares-plugin xfce4 xfce4-goodies")

    # ---Function install LxQT
    def install_lxqt(self, button):
        os.system("xterm -e sudo pacman -S lxqt breeze-icons")

    # ---Function remove LxQT
    def remove_lxqt(self, button):
        os.system("xterm -e sudo pacman -Runs lxqt breeze-icons")

    # ---Function install Gnome
    def install_gnome(self, button):
        os.system("xterm -e sudo pacman -S gnome gnome-tweaks")

    # ---Function remove Gnome
    def remove_gnome(self, button):
        os.system("xterm -e sudo pacman -Runs gnome gnome-extra gnome-tweaks")

    # ---Function install KDE
    def install_kde(self, button):
        os.system("xterm -e sudo pacman -S plasma-meta konsole kate dolphin dolphin-plugins")

    # ---Function remove KDE
    def remove_kde(self, button):
        os.system("xterm -e sudo pacman -Rs plasma-meta konsole kate dolphin dolphin-plugins")
        


win = GridWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
