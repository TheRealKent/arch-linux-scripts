#!/usr/bin/env python3
#CLI Control Center V0.4
import csv
import sys
import os
import datetime

def main():
    os.system("clear")
    menu()

#the menu
def menu():
    now = datetime.datetime.now()
    print(now.strftime("[%H:%M]"),3 * "-", "Control Center V0.4", 10 * "-")
    choice = input("""
1: Install Nvidia       | 5: Updater
2: System info          | 6: Firewall
3: Theme/icon installer | 7: Apparmor
4: Cleaner              | 

Please enter your choice (q to quit): """)

    if choice == "1":
        nvidia_installer()
    elif choice == "2":
        sysinfo()
    elif choice == "3":
        theme_icon_installer()
    elif choice == "4":
        clean()
    elif choice == "5":
        updatesystem()
    elif choice == "6":
        firewall()
    elif choice == "7":
        apparmor()
    elif choice== "Q" or choice== "q":
        os.system("clear")
        sys.exit
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()

#Finction to update the sustem
def updatesystem():
   os.system("sudo pacman -Syu && yay -Syu")
   os.system("clear")
   menu()

#Function to clean the system
def clean():
   os.system("sudo pacman -Scc")
   os.system("clear")
   menu()

#Finction to install icons and themes
def theme_icon_installer():
   ti_input = input("""
1: Install Theme
2: install Icon

Please enter your choice (q to go back): """)

   if ti_input == "1":
        theme_folder = input("""
path to theme folder you want to install: """)
        tfo = '/usr/share/themes/'
        test = "sudo mv -v "+theme_folder+" "+tfo
        os.system(test)
        os.system("clear")
        menu()
   elif ti_input == "2":
        icon_folder = input("""
path to icon folder you want to install: """)
        ifo = '/usr/share/icons/'
        test = "sudo mv -v "+icon_folder+" "+ifo
        os.system(test)
        os.system("clear")
        menu()
   elif ti_input == "Q" or ti_input == "q":
        os.system("clear")
        menu()
   else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()

#Finction to manage the apparmor
def apparmor():
   app_input = input("""
1: Status
2: Start
3: Stop

Please enter your choice (q to go back): """)

   if   app_input == "1":
        os.system("sudo aa-status")
        apparmor()
   elif app_input == "2":
        os.system("sudo systemctl enable apparmor && sudo systemctl start apparmor")
        apparmor()
   elif app_input == "3":
        os.system("sudo systemctl stop apparmor && sudo systemctl disable apparmor")
        apparmor()
   elif app_input == "Q" or app_input == "q":
        os.system("clear")
        menu()
   else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        apparmor()

#Finction to manage the Firewall
def firewall():
   fw_input = input("""
1: Status
2: Start
3: Stop

Please enter your choice (q to go back): """)

   if   fw_input == "1":
        os.system("sudo ufw status verbose")
        firewall()
   elif fw_input == "2":
        os.system("sudo ufw enable")
        os.system("sudo ufw status verbose")
        firewall()
   elif fw_input == "3":
        os.system("sudo ufw disable")
        os.system("sudo ufw status verbose")
        firewall()
   elif fw_input == "Q" or fw_input == "q":
        os.system("clear")
        menu()
   else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        firewall()

#Finction to install the nvidia driver
def nvidia_installer():
   ti_input = input("""
1: Install Nvidia driver
2: UnInstall Nvidia driver

Please enter your choice (q to go back): """)

   if ti_input == "1":
        os.system("sudo pacman -S nvidia lib32-nvidia-utils libvdpau nvidia-utils nvidia-settings xorg-server-devel opencl-nvidia")
        os.system("clear")
        menu()
   elif ti_input == "2":
        os.system("sudo pacman -R nvidia lib32-nvidia-utils libvdpau nvidia-utils nvidia-settings xorg-server-devel opencl-nvidia")
        os.system("clear")
        menu()
   elif ti_input == "Q" or ti_input == "q":
        os.system("clear")
        menu()
   else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()

def sysinfo():
   os.system("inxi -F")
   menu()
#the program is initiated, so to speak, here
main()
