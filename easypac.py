#!/usr/bin/env python3
#Easy pacman manager CLI V0.1
import csv
import sys
import os
import datetime

def main():
    os.system("clear")
    menu()

#the menu
def menu():
    now = datetime.datetime.now()
    print(now.strftime("[%H:%M]"),3 * "-", "EasyPack V0.1", 10 * "-")
    choice = input("""
1: Install
2: Remove 
3: Search
4: Cleaner 
5: Update

Please enter your choice (q to quit): """)

    if choice == "1":
        install()
    elif choice == "2":
        remove()
    elif choice == "3":
        search()
    elif choice == "4":
        clean()
    elif choice == "5":
        updatesystem()    
    elif choice== "Q" or choice== "q":
        os.system("clear")
        sys.exit
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()

#Finction to update the sustem
def updatesystem():
   os.system("sudo pacman -Syu")
   os.system("clear")
   menu()

#Function to clean the system
def clean():
   os.system("sudo pacman -Scc")
   os.system("clear")
   menu()

#Finction to install a pkg
def install():
   ti_input = input("""
1: Install a pkg

Please enter your choice (q to go back): """)

   if ti_input == "1":
        pkg_in_name = input("""
Enter pkg name: """)
        test = "sudo pacman -S "+pkg_in_name
        os.system(test)
        os.system("clear")
        menu()
   elif ti_input == "Q" or ti_input == "q":
        os.system("clear")
        menu()
   else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()

#Finction to remove a pkg
def remove():
   pr_input = input("""
1: Remove a pkg

Please enter your choice (q to go back): """)

   if pr_input == "1":
        pkg_re_name = input("""
Enter pkg name: """)
        test = "sudo pacman -Runs "+pkg_re_name
        os.system(test)
        os.system("clear")
        menu()
   elif ti_input == "Q" or pr_input == "q":
        os.system("clear")
        menu()
   else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()

#Finction to remove a pkg
def search():
   pkg_se_name = input("""
Enter pkg name: """)
   test = "sudo pacman -Ss "+pkg_se_name
   os.system(test)
   menu()
  
#the program is initiated, so to speak, here
main()
